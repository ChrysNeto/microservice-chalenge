# Libraries
from app import app
from flask import make_response, jsonify, request

# Create MongoDB Database 
#mongodb = MongoDbDatabase(port = os.environ['MONGO_PORT'],
#                          hostname = os.environ['MONGO_HOSTNAME'],
#                          database_name = os.environ['MONGO_INITIAL_DB'],
#                          connector_username = os.environ['MONGO_USERNAME'],
#                          connector_password = os.environ['MONGO_ROOT_PASSWORD'])

# Base path
@app.route("/")
def index():
    return "<h1 style='color:green'>Microservice for logs UP! =D</h1>"

# /logs path, METHOD = POST
@app.route("/logs", methods=['POST'])
def fetch_logs():
    #db = mongodb.get_db()
    return make_response(jsonify({"status": "Up!"}),200)
