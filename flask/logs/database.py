class MongoDbDatabase:
    """Create MongoDB database"""

    def __init__(self, port: int, hostname: str, database_name: str, connector_username: str, connector_password: str):
        """Constructor for the class"""
        super().__init__() 
        self.hostname = hostname
        self.port = port
        self.database_name = database_name
        self.connector_username = connector_username    
        self.connector_password = connector_password
    
    def get_db(self):
        """Setup connector for the database"""
        client = MongoClient(host=self.hostname,
                             port=self.port,
                             user=self.connector_username,
                             password=self.connector_password,
                             authSource='admin')
        db = client(self.database_name)
        return db